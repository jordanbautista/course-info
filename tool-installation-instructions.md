# Configuring Your Machine to Compile C Code

## Configure Environment

Most of the tools we will use for this class were built for Unix. They are mostly incompatible with Windows, and mostly compatible with Mac OS. We can make Windows compatibility better by installing a compatibility layer known as the Windows Subsystem for Linux (WSL).

## Windows 10

- Install the Windows Subsystem for Linux by following these instructions: https://docs.microsoft.com/en-us/windows/wsl/install-win10
- Install gcc: `sudo apt-get update; sudo apt-get install gcc`
- Enter the password you chose when prompted
- Type `Y` if prompted

## Mac OS

- Download X Code from the Apple App Store. We need it for the compiler it installs
- You'll need OS X version 10.13.6 or higher.
- If you can't find the download in the app store, it means your version of Mac OS is too old, and you'll need to download it manually here: https://support.apple.com/kb/DL1969?locale=en_US


## Git Installation

We will use Git for version control. This will help you keep track of changes, ask for help, and test your code. Follow the instructions here: https://git-scm.com/downloads

## Install Visual Studio Code

For most assignments, we will use Visual Studio Code to write and run applications. Download and install here: https://code.visualstudio.com
